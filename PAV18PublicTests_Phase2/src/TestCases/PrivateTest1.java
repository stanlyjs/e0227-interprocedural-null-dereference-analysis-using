package TestCases;

/*
 * To sort a linked list using insertion sort
 */

public class PrivateTest1 
{ 
	//Node head; 		/* Input list */
	//Node sorted; 	/* Temporary list for sorting */

	Node push(Node head, int val) 
	{ 
		/* insert a new Node at the beginning */
		Node newnode = new Node();
		Data tmpD = new Data(val);
		
		newnode.data = tmpD;
		newnode.next = head; 
		head = newnode;
		
		return head;
	} 

	/*
	* recursive 	function to sort a singly linked list using insertion sort
	*/ 
	Node insertionSort(Node headref, Node sorted) 
	{ 
		Node current = headref;
		
		if (current != null) {
			Node rest = current.next;
			sorted = insert(sorted, current);
			sorted = insertionSort(rest, sorted);
		}
		
		return sorted;
	} 

	/* 
	* function to insert a new Node in already sorted list. 
	*/
	Node insert(Node sorted, Node newnode) 
	{ 
		int sData = sorted.data.get();
		int nData = newnode.data.get();
		
		if (sorted == null || sData >= nData) 
		{ 
			newnode.next = sorted; 
			sorted = newnode; 
		} 
		else
		{ 
			Node current = sorted;
			sData = current.next.data.get();
			nData = newnode.data.get();

			/* Locate the node before the point of insertion */
			while (current.next != null && sData < nData) 
			{ 
				current = current.next;
				sData = current.next.data.get();
			} 
			newnode.next = current.next; 
			current.next = newnode; 			
		} 
		return sorted;
	} 

	/*  
	 * Create list and sort 
	 */
	public void startTest() 
	{ 
		PrivateTest1 list = new PrivateTest1(); 
		Node head = null;
		
		head = list.push(head, 5); 
		head = list.push(head, 20); 
		head = list.push(head, 4); 
		head = list.push(head, 3); 
		head = list.push(head, 30); 
		
		head = list.insertionSort(head, null); 
	} 
} 