// Implementation of tree using array numbering starting from 0 to n-1.

package TestCases;

class PrivateTest2 {
	
	/*create root*/
	public void Root(Node[] t, Node key) 
	{ 
		t[0] = key;
	} 

	/*create left son of root*/
	public void Left(Node t[], Node key, int root) 
	{ 
		int tmp = (root * 2) + 1; 

		if(t[root] == null){ 
			; 
		}else{ 
			t[tmp] = key; 
		} 
	} 

	/*create right son of root*/
	public void Right(Node t[], Node key, int root) 
	{ 
		int tmp = (root * 2) + 2; 

		if(t[root] == null){ 
			; 
		}else{ 
			t[tmp] = key; 
		} 
	} 

	public void startTestarray() 
	{ 
		Node[] tree = new Node[10];
		
		PrivateTest2 obj = new PrivateTest2(); 
		Node root = new Node();
		Data rootD = new Data(1);
		root.data = rootD;
		
		Node l1 = new Node();
		Data l1D = new Data(6);
		l1.data = l1D;
		
		Node r1 = new Node();
		Data r1D = new Data(3);
		r1.data = r1D;
		
		Node l2 = new Node();
		Data l2D = new Data(5);
		l2.data = l2D;
						
		obj.Root(tree, root); 
		obj.Left(tree, l1, 0); 
		obj.Right(tree, r1, 0); 
		obj.Left(tree, l2, 1); 
		obj.Right(tree, null, 1); 
	} 
 
} 