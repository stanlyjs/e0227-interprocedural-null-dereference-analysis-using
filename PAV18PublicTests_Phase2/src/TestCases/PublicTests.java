/*
 * @author Stanly Samuel and Rekha Pai
 */

package TestCases;

public class PublicTests {
	
	public static void main(String[] args) {
		Test1 t1 = new Test1();
		t1.foo();
		Test2 t2 = new Test2();
		t2.array();
		PrivateTest1 pt1 = new PrivateTest1();
		pt1.startTest();
		PrivateTest2 pt2 = new PrivateTest2();
		pt2.startTestarray();
		PrivateTest3 pt3 = new PrivateTest3();
		pt3.foo3();
		PrivateTest4 pt4 = new PrivateTest4();
		pt4.startTest4();
	}
}
