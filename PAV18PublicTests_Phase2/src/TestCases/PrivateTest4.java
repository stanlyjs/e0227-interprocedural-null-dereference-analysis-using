package TestCases;

public class PrivateTest4 {
	
	public void startTest4() {
		PublicTests[] arr_test = new PublicTests[3];
		
		PublicTests test1, test2, test3;
		
		test1 = new PublicTests();
		test3 = test1;
		test2 = null;
		
		arr_test[0] = test1;
		arr_test[1] = test2;
		arr_test[2] = test3;
		
		arr_test[1] = new PublicTests();
		arr_test[2] = arr_test[1];
		
		arr_test.toString();
		

		arr_test = new PublicTests[5];
		int i = 0;
		for(i = 0; i < 5; i++)
			arr_test[i] = new PublicTests();
		
		for(i = 0; i < 4; i++)
			arr_test[i] = arr_test[i+1];
		
        arr_test.toString();
	}
}