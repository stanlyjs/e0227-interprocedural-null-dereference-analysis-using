/*
 * @author Stanly Samuel and Dr. Rekha Pai
 */
package PAVNullDerefPackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.io.FileProvider;

public class SetUpAnalysis {

	private String classpath;
	private String mainClass;
	private String analysisClass;
	private String analysisMethod;
	private static HashMap<CGNode, HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>>> result = new LinkedHashMap<CGNode, HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>>>();
    private static HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> columnMap = new LinkedHashMap<Integer, HashMap<Value, ArrayList<Tuple>>>();

	// START: NO CHANGE REGION
	private AnalysisScope scope; // scope defines the set of files to be analyzed
	private ClassHierarchy ch; // Generate the class hierarchy for the scope
	private Iterable<Entrypoint> entrypoints; // In the call graph, these are the entrypoint nodes
	private AnalysisOptions options; // Specify options for the call graph builder
	private CallGraphBuilder builder; // Builder object for the call graph
	private CallGraph cg; // Call graph for the program

	public SetUpAnalysis(String classpath, String mainClass, String analysisClass, String analysisMethod) {
		this.classpath = classpath;
		this.mainClass = mainClass;
		this.analysisClass = analysisClass;
		this.analysisMethod = analysisMethod;
	}

	/**
	 * Defines the scope of analysis: identifies the classes for analysis
	 * 
	 * @throws Exception
	 */
	public void buildScope() throws Exception {
		FileProvider f = new FileProvider();
		scope = AnalysisScopeReader.makeJavaBinaryAnalysisScope(classpath,
				f.getFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS));
	}

	/**
	 * Builds the hierarchy among the classes to be analyzed: if B extends A, then A
	 * is a superclass of B, etc
	 * 
	 * @throws Exception
	 */
	public void buildClassHierarchy() throws Exception {
		ch = ClassHierarchy.make(scope);
	}

	/**
	 * The nodes of a call graph are methods. This method defines the "entry points"
	 * in the call graph. Note: The entry point may not necessarily be the main
	 * method.
	 */
	public void buildEntryPoints() {
		entrypoints = Util.makeMainEntrypoints(scope, ch, mainClass);
	}

	/**
	 * Options to build the required call graph.
	 */
	public void setUpCallGraphConstruction() {
		options = CallGraphTestUtil.makeAnalysisOptions(scope, entrypoints);
		builder = Util.makeZeroCFABuilder(options, new AnalysisCache(), ch, scope);
	}

	/**
	 * Build the call graph.
	 * 
	 * @throws Exception
	 */
	public void generateCallGraph() throws Exception {
		cg = builder.makeCallGraph(options, null);
	}

	/**
	 * These are methods for testing purposes. You can call to these to check
	 * whether Wala is setup properly. This method prints the nodes of the call
	 * graph.
	 */
	public void printNodes() {
		System.out.println("Displaying Application's Call Graph nodes: ");
		Iterator<CGNode> nodes = cg.iterator();

		// Printout the nodes in the call-graph
		while (nodes.hasNext()) {
			String nodeInfo = nodes.next().toString();
			if (nodeInfo.contains("Application"))
				System.out.println(nodeInfo);
		}
	}

	/**
	 * This method prints the IR of the analysisMethod
	 */
	public void printIR() {
		System.out.println("\n\n");
		Iterator<CGNode> nodes = cg.iterator();
		CGNode target = null;
		while (nodes.hasNext()) {
			CGNode node = nodes.next();
			String nodeInfo = node.toString();
			if (nodeInfo.contains(analysisClass) && nodeInfo.contains(analysisMethod)) {
				target = node;
				break;
			}
		}
		if (target != null) {
			System.out.println("The IR of method " + target.getMethod().getSignature() + " is:");
			System.out.println(target.getIR().toString());
		} else {
			System.out.println("The given method in the given class could not be found");
		}
	}

	// END: NO CHANGE REGION

	// Intraprocedural analysis using Algorithm A
	public void analyze() {
		
		Iterator<CGNode> nodes = cg.iterator();
		CGNode target = null;
		while (nodes.hasNext()) {
			CGNode node = nodes.next();
			String nodeInfo = node.toString();
			if (nodeInfo.contains(analysisClass) && nodeInfo.contains(analysisMethod)) {
				target = node;
				break;
			}
		}
		if (target != null) {
				SymbolTable symbolTable = target.getIR().getSymbolTable();
				columnMap.put(0, new HashMap<Value, ArrayList<Tuple>>());
				analyze_Iterative(target, 0, symbolTable);

		}
	}

	public HashMap<Value, ArrayList<Tuple>> analyze_Iterative(CGNode target, Integer columnNumber, SymbolTable symbolTable) {
		
		HashMap<Value, ArrayList<Tuple>> returnJop = new HashMap<Value, ArrayList<Tuple>>();
		
		System.out.println("--------------------------------------------------------------------------------------------------------CALL-----------------------------------------------------------------------------------");
		//Function is called with column i.
		//Case 1: Function has never been called before (First call or Recursion or normal call) -> Initialize every edge's column number with i and new object.
		//Case 2: Function has been called but with new column (Recursion or normal call) -> 
		//Case 3: Function has been called but with existing column (Recursion or normal call) -> Just check for some edge if column i exists, if yes then break.//Taken care of while calling function
		
		if(result.get(target)==null) // Function was never called
		{
			result.put(target, new HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>>());
		}
		
		//If reached till here then, function has been called with new column, hence this column must be added for all edges.
		
		SSACFG cfg = target.getIR().getControlFlowGraph();

		Queue<Edge> edgeQueue = new LinkedList<Edge>();

		Queue<SSACFG.BasicBlock> blockQueue = new LinkedList<SSACFG.BasicBlock>();

		blockQueue.add(cfg.entry());

		// Add all edges to a queue (edgeQueue) and initial hashMap of these edges to
		// null
		while (!blockQueue.isEmpty()) {
			SSACFG.BasicBlock block = blockQueue.remove();

			Iterator<ISSABasicBlock> cfgIt = cfg.getSuccNodes(block);

			while (cfgIt.hasNext()) {
				SSACFG.BasicBlock block2 = (SSACFG.BasicBlock) cfgIt.next();

				int src = block.getGraphNodeId();
				int dest = block2.getGraphNodeId();
				Edge e = new Edge(src, dest);

				if (!edgeQueue.contains(e) && !cfg.hasExceptionalEdge(block, block2)) {
					edgeQueue.add(e);
					
					HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge;
					if(result.get(target).get(e)==null)
					{
						newColumnForEdge = new HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>();
					}
					else
					{
						newColumnForEdge = result.get(target).get(e);
					}
						
					newColumnForEdge.put(columnNumber, new HashMap<Value, ArrayList<Tuple>>()); //Added new column
					
					HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
					funJop.put(e, newColumnForEdge);
					
					result.put(target, funJop);
					
					blockQueue.add(block2);
				}
				
			}
			
		}
		//So far all edges in function have the current column as empty. Now we just need to fill this column

		while (!edgeQueue.isEmpty()) {

			Edge edge = edgeQueue.remove();
			HashMap<Value, ArrayList<Tuple>> JOP ;
			if(edge.src()==0) //For edge 0,1, input value at column number will be the inpput by default. For first function it will be empty
			{
//				System.out.println(columnMap);
//				System.out.println("Column number"+columnNumber);
				JOP = new HashMap<Value, ArrayList<Tuple>>(columnMap.get(columnNumber));				
						
				HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(edge); //wont be null
				newColumnForEdge.put(columnNumber, JOP);
				HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
				funJop.put(edge, newColumnForEdge);
				result.put(target, funJop);
			}
			else
			{
				JOP = new HashMap<Value, ArrayList<Tuple>>(result.get(target).get(edge).get(columnNumber)); // Copy Constructor
			}
			SSACFG.BasicBlock destBB = cfg.getBasicBlock(edge.dest());

			List<SSAInstruction> instList = destBB.getAllInstructions();
			Iterator<SSAInstruction> instIt = instList.iterator();

			if (instList.isEmpty()) // EMPTY BASIC BLOCK
			{
				for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
					Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

					HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

					HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);

					newJOP = join(newJOP, outEdgeJOP);
					if (OverApprox(outEdgeJOP, newJOP)) {
						
						HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
						newColumnForEdge.put(columnNumber, newJOP);
						HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
						funJop.put(e, newColumnForEdge);
						result.put(target, funJop);
						
						edgeQueue.add(e);
					}
				}
			}

			while (instIt.hasNext()) {
				SSAInstruction inst = instIt.next();

				if (inst instanceof SSANewInstruction) {

					SSANewInstruction snew = (SSANewInstruction) inst;
					Value v = new Value(snew.getDef());
					ArrayList<Tuple> arr = new ArrayList<Tuple>();
					arr.add(new Tuple(snew.iindex,target.getMethod().getName().toString()));

					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);
						newJOP.put(v, arr);

						newJOP = join(newJOP, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJOP)) {
							
							//Phase 2
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJOP);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							
							//result.put(e, newJOP); Phase 1
							edgeQueue.add(e);
						}
					}
				}

				else if (inst instanceof SSAGetInstruction) {
					SSAGetInstruction snew = (SSAGetInstruction) inst;
					HashMap<Value, ArrayList<Tuple>> newJop;
					newJop = calculateJop(JOP, snew);

					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						newJop = join(newJop, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJop)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJop);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}					
				}

				else if (inst instanceof SSAPutInstruction) {
					
					SSAPutInstruction snew = (SSAPutInstruction) inst;
					HashMap<Value, ArrayList<Tuple>> newJop;
					newJop = calculateJop(JOP, snew, symbolTable);

					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						newJop = join(newJop, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJop)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJop);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}
				}

				else if (inst instanceof SSAPhiInstruction) {
					SSAPhiInstruction snew = (SSAPhiInstruction) inst;
					HashMap<Value, ArrayList<Tuple>> newJop;
					newJop = calculateJop(JOP, snew, symbolTable);
					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);
//						System.out.println(outEdgeJOP);
						newJop = join(newJop, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJop)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJop);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}
				}

				else if (inst instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction snew = (SSAInvokeInstruction) inst;
					
					
					if(!snew.isSpecial() && !snew.isStatic() && snew.getDeclaredTarget().getDeclaringClass().toString().contains("Application"))
					{
						System.out.println("TEST");
						System.out.println(snew);
						System.out.println(snew);
//						if (snew.getDeclaredTarget().getDeclaringClass().isReferenceType()) {
//							HashMap<Value, ArrayList<Tuple>> newJop;
//							newJop = calculateJop(JOP, snew, target);
//							for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
//								Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
//
//								HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);
//
//								newJop = join(newJop, outEdgeJOP);
//								if (OverApprox(outEdgeJOP, newJop)) {
//									HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
//									newColumnForEdge.put(columnNumber, newJop);
//									HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
//									funJop.put(e, newColumnForEdge);
//									result.put(target, funJop);
//									edgeQueue.add(e);
//								}
//							}
//						}
						//Find function in call graph
						System.out.println(snew);
						Iterator<CGNode> nodes = cg.iterator();
						CGNode newTarget = null;
						while (nodes.hasNext()) {
							CGNode node = nodes.next();
							String nodeInfo = node.toString();
							if (nodeInfo.contains(analysisClass) && nodeInfo.contains(snew.getDeclaredTarget().getName().toString())) {
								newTarget = node;
								break;
							}
						}
						
						HashMap<Value, ArrayList<Tuple>> intJOP = new HashMap<Value, ArrayList<Tuple>>();
						//1) Copy non local variables and objects to intJOP
						System.out.println("intJOP"+intJOP);
						for(Value val: JOP.keySet())
						{
							if(!val.isXVal)
							{
								intJOP.put(val, JOP.get(val));
							}
						}
						System.out.println("intJOP_AFTER"+intJOP);
						//2) Stitch local variables of called function and put them into intJOP
						System.out.println(snew.getNumberOfUses());
						for(int i=0; i<snew.getNumberOfUses();i++)
						{
							System.out.println("USE"+i+"="+snew.getUse(i));
						}
						for(int i=0; i<snew.getNumberOfParameters();i++)
						{
							System.out.println("Parameter type"+i+"="+newTarget.getMethod().getParameterType(i)); //assumes snew and newtarget has same parameters (MUST HAVE)
						}
						
						for(int i=1;i<snew.getNumberOfUses(); i++)
						{
							if(newTarget.getMethod().getParameterType(i).isReferenceType())
							{
								intJOP.put(new Value(i+1), JOP.get(new Value(snew.getUse(i))));
							}
						}
						System.out.println("intJOP_AFTERADD"+intJOP);
						
						//3 Check if intJOP already present in ColumnTable
						//if not present then it is brand new and and must be added in column table AND passed to function.
						//else if present in column table, get column table number and check if it this number is present in at least one edge in the called function.
						//If present, then don't call function (fixpoint) else call function with this value.
						
						HashMap<Value, ArrayList<Tuple>> newJop = null;

						if(columnMap.containsValue(intJOP))
						{
							newJop = new HashMap<Value, ArrayList<Tuple>>();
							//will be needed for recursion
						}
						else
						{
//							System.out.println("Column map size"+columnMap.size());
							columnMap.put(columnMap.size(), intJOP);
							System.out.println("----------------Before Call-------------");
							newJop = analyze_Iterative(newTarget, columnMap.size()-1, newTarget.getIR().getSymbolTable()); //columnmap size - 1 since I just added an element in a previous step
							System.out.println("----------------After Call-------------");
						}
						System.out.println("RETURNEDJOP"+newJop);
						
						
						
						System.out.println("JOP"+JOP);
						
						System.out.println(newTarget.getMethod().getParameterType(2));
						
						
						//4) Postprocessing: Whenever you see a value with -2, replace it with the return variable.
						
						System.out.println("RETURNED RECIEVER"+snew.getReturnValue(0));
						ArrayList<Tuple> pointsTo = new ArrayList<Tuple>();
						
						for(Value val: newJop.keySet())
						{
							if(val.isXVal && val.x==-2)
							{
								pointsTo = new ArrayList<Tuple>(newJop.get(val));
							}
						}
						
						if(newJop.containsKey(new Value(-2)))
						newJop.remove(new Value(-2));
						
						newJop.put(new Value(snew.getReturnValue(0)), pointsTo);
						
						System.out.println("RETURNEDAFTERPROCESSING"+newJop);
						
						for(Value val: JOP.keySet())
						{
							if(val.isXVal)
							{
								newJop.put(val, JOP.get(val));
							}
						}
						
						System.out.println("RETURNEDAFTERPROCESSING2"+newJop);
						
						for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
							Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());

							HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

							newJop = join(newJop, outEdgeJOP);
							if (OverApprox(outEdgeJOP, newJop)) {
								HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
								newColumnForEdge.put(columnNumber, newJop);
								HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
								funJop.put(e, newColumnForEdge);
								result.put(target, funJop);
								edgeQueue.add(e);
							}
						}
						if (newTarget != null) {
							System.out.println("The IR of method " + newTarget.getMethod().getSignature() + " is:");
							System.out.println(newTarget.getIR().toString());
						} else {
							System.out.println("The given method in the given class could not be found");
						}
					}
						

					
					else {// Identity
						for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
							Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
							// System.out.println(*);
							HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

							HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);

							newJOP = join(newJOP, outEdgeJOP);
							if (OverApprox(outEdgeJOP, newJOP)) {
								HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
								newColumnForEdge.put(columnNumber, newJOP);
								HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
								funJop.put(e, newColumnForEdge);
								result.put(target, funJop);
								edgeQueue.add(e);
							}

						}
					}
				} 
				
				else if (inst instanceof SSAReturnInstruction)
				{
					SSAReturnInstruction snew = (SSAReturnInstruction) inst;			
						
					System.out.println("RETURN JOP INPUT"+JOP);
					HashMap<Value, ArrayList<Tuple>> intJOP = new HashMap<Value, ArrayList<Tuple>>();
					
					//1) Add dummy value -2 as placemarker for whatever this function will return 
					
					System.out.println("RETURN USE"+snew.getResult());
					for(Value val: JOP.keySet())
					{
						if(val.isXVal && val.x ==snew.getResult())
						{
							intJOP.put(new Value(-2), JOP.get(val));
						}
					}
					//2) Copy non local variables and objects to intJOP
					System.out.println("intJOP"+intJOP);
					for(Value val: JOP.keySet())
					{
						if(!val.isXVal)
						{
							intJOP.put(val, JOP.get(val));
						}
					}
					System.out.println("intJOP_AFTER"+intJOP);
					
					returnJop= join(returnJop, intJOP);
					

						//3 Check if intJOP already present in ColumnTable
						//if not present then it is brand new and and must be added in column table AND passed to function.
						//else if present in column table, get column table number and check if it this number is present in at least one edge in the called function.
						//If present, then don't call function (fixpoint) else call function with this value.

					}
					
				
				else if (inst instanceof SSAArrayStoreInstruction) {
					SSAArrayStoreInstruction snew = (SSAArrayStoreInstruction) inst;
					HashMap<Value, ArrayList<Tuple>> newJop;
					newJop = calculateJop(JOP, snew);
					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
						// System.out.println(*);

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						newJop = join(newJop, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJop)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJop);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}
				} else if (inst instanceof SSAArrayLoadInstruction) {
					SSAArrayLoadInstruction snew = (SSAArrayLoadInstruction) inst;
					HashMap<Value, ArrayList<Tuple>> newJop;
					newJop = calculateJop(JOP, snew);
					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
						// System.out.println(*);

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						newJop = join(newJop, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJop)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJop);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}

				}
//
//				else if (inst instanceof SSAConditionalBranchInstruction) {
//					SSAConditionalBranchInstruction snew = (SSAConditionalBranchInstruction) inst;
//					ArrayList<Tuple> lhsPointsTo = JOP.get(snew.getUse(0));
//					ArrayList<Tuple> rhsPointsTo = JOP.get(snew.getUse(1));
//
//					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
//						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
//
//						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(e);
//
//						HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);
//
//						newJOP = join(newJOP, outEdgeJOP);
//						if (OverApprox(outEdgeJOP, newJOP)) {
//							//// System.out.println(*);
//							result.put(e, newJOP);
//							edgeQueue.add(e);
//						}
//					}
//				}
//
				else // Identity Transfer function
				{
					for (ISSABasicBlock outDestBB : cfg.getNormalSuccessors(cfg.getBasicBlock(edge.dest()))) {
						Edge e = new Edge(edge.dest(), outDestBB.getGraphNodeId());
						// System.out.println(*);

						HashMap<Value, ArrayList<Tuple>> outEdgeJOP = result.get(target).get(e).get(columnNumber);

						HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);

						newJOP = join(newJOP, outEdgeJOP);
						if (OverApprox(outEdgeJOP, newJOP)) {
							HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> newColumnForEdge = result.get(target).get(e); //wont be null
							newColumnForEdge.put(columnNumber, newJOP);
							HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>> funJop = result.get(target); //wont be null
							funJop.put(e, newColumnForEdge);
							result.put(target, funJop);
							edgeQueue.add(e);
						}
					}
				}
			}
		}
		if(target.getMethod().getName().toString().equalsIgnoreCase(analysisMethod))
		{
			printColumns(columnMap);
			printInterProceduralResult(result);
		}
			
		System.out.println("--------------------------------------------------------------------------------------------------------RETURN-----------------------------------------------------------------------------------");
		return returnJop; 
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> JOP,
			SSAGetInstruction snew) {
		HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);
		Value def = new Value(snew.getDef());
		String field = snew.getDeclaredField().getName().toString();
		ArrayList<Tuple> ar = JOP.get(new Value(snew.getUse(0)));
		if (snew.isStatic()) {
			String classname = snew.getDeclaredField().getDeclaringClass().getName().toString();

			if (JOP.containsKey(new Value(classname, field))) {
				ArrayList<Tuple> temp = JOP.get(new Value(classname, field));
				newJOP.put(def, temp);
			} else {
				return JOP;
			}
		} else {
			if (ar == null) {
				return JOP;
			}

			HashSet<Tuple> hs = new HashSet<Tuple>();

			for (Tuple i : ar) {
				if (JOP.containsKey(new Value(i.getContext(), i.getValue(), field))) {
					ArrayList<Tuple> temp = JOP.get(new Value(i.getContext(), i.getValue(), field));
					hs.addAll(temp);
				}
			}
			ar = new ArrayList<Tuple>(hs);
			if (ar.size() != 0)
				newJOP.put(def, ar);
		}
		return newJOP;
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> jOP,
			SSAArrayLoadInstruction snew) {
		HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(jOP);
		int def = snew.getDef();
		int use = snew.getUse(0);
		ArrayList<Tuple> ar = jOP.get(new Value(use));
		HashSet<Tuple> hs = new HashSet<Tuple>();
		for (Tuple x : ar) {

			if (jOP.containsKey(new Value(x.getContext(), x.getValue(), ""))) {
				hs.addAll(jOP.get(new Value(x.getContext(), x.getValue(), "")));
			}
		}
		if (hs.isEmpty()) {
			return jOP;
		}
		ArrayList<Tuple> temp = new ArrayList<Tuple>(hs);
		newJOP.put(new Value(def), temp);
		return newJOP;
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> jOP,
			SSAArrayStoreInstruction snew) {
		HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(jOP);
		int def = snew.getUse(0);
		int use = snew.getUse(2);
		if (!jOP.containsKey(new Value(use))) {
			return jOP;
		}
		ArrayList<Tuple> ar = jOP.get(new Value(use));
		ArrayList<Tuple> defList = jOP.get(new Value(def));
		if (defList == null) {
			return jOP;
		}
		if (defList.size() == 0) {
			return jOP;
		}
		for (Tuple x : defList) {
			if (jOP.containsKey(new Value(x.getContext(), x.getValue(), ""))) {
				ArrayList<Tuple> temp = jOP.get(new Value(x.getContext(), x.getValue(), ""));
				HashSet<Tuple> hs = new HashSet<Tuple>();
				hs.addAll(temp);
				hs.addAll(ar);
				temp = new ArrayList<Tuple>(hs);
				newJOP.put(new Value(x.getContext(), x.getValue(), ""), temp);
			} else {
				newJOP.put(new Value(x.getContext(), x.getValue(), ""), ar);
			}
		}
		return newJOP;
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> JOP,
			SSAInvokeInstruction snew, CGNode target) {
		HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);
		Value def = new Value(snew.getDef());
		ArrayList<Tuple> temp = new ArrayList<Tuple>();
		temp.add(new Tuple(snew.iindex, target.getMethod().getName().toString()));
		newJOP.put(def, temp);
		return newJOP;
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> JOP,
			SSAPhiInstruction snew, SymbolTable symbolTable) {
		Value def = new Value(snew.getDef());
		HashSet<Tuple> x = new HashSet<Tuple>();

		for (int i = 0; i < snew.getNumberOfUses(); i++) {
			if (symbolTable.isNullConstant(snew.getUse(i))) {
				x.add(new Tuple(-1,"")); // Assuming in phi node if a variable vi doesnt point to anything, then it MUST
										// be assigned constant NULL. Verify
				// Add vi->NULL as well separately
				Value v = new Value(snew.getUse(i));
				ArrayList<Tuple> arr = new ArrayList<Tuple>();
				arr.add(new Tuple(-1,""));
				JOP.put(v, arr);
			}
			if (JOP.get(new Value(snew.getUse(i))) == null) {

			} else
				x.addAll(JOP.get(new Value(snew.getUse(i))));
		}
		ArrayList<Tuple> ar = new ArrayList<Tuple>();
		ar.addAll(x);
//		Collections.sort(ar);
		HashMap<Value, ArrayList<Tuple>> newJop = new HashMap<Value, ArrayList<Tuple>>(JOP);
		if (ar.size() > 0)
			newJop.put(def, ar);
		return newJop;
	}

	private HashMap<Value, ArrayList<Tuple>> calculateJop(HashMap<Value, ArrayList<Tuple>> JOP,
			SSAPutInstruction snew, SymbolTable symbolTable) {
		String field = snew.getDeclaredField().getName().toString();
		Value def = new Value(snew.getRef());
		Value use = new Value(snew.getUse(1));
		HashMap<Value, ArrayList<Tuple>> newJOP = new HashMap<Value, ArrayList<Tuple>>(JOP);

		if (snew.isStatic()) {
			ArrayList<Tuple> ar = JOP.get(use);
			if (ar == null) {
				return JOP;
			}
			if (ar.size() == 0) {
				return JOP;
			}
			String classname = snew.getDeclaredField().getDeclaringClass().getName().toString();

			Value classnameVal = new Value(classname, field);
			if (JOP.containsKey(classnameVal)) {
				HashSet<Tuple> hs = new HashSet<Tuple>(ar);
				hs.addAll(JOP.get(classnameVal));
				ArrayList<Tuple> temp = new ArrayList<Tuple>(hs);
				newJOP.put(classnameVal, temp);
			} else {
				newJOP.put(classnameVal, ar);
			}
		} else {
			ArrayList<Tuple> ar = JOP.get(use);

			if (!snew.getDeclaredFieldType().isReferenceType()) // For putfields of type v11.i = v12 and i is of type
																// int (v12 wont exist and we will make it point to null
																// unnecessarily)
			{
				return JOP;
			}

			if (ar == null) // of form v8:NULL so add -1 in points to set and add (v8,-1) as well
			{
				if (symbolTable.isNullConstant(snew.getUse(1))) {
					// Add vi->NULL as well separately
					Value v = new Value(snew.getUse(1));
					ArrayList<Tuple> arr = new ArrayList<Tuple>();
					arr.add(new Tuple(-1,""));
					newJOP.put(v, arr);
					System.out.println(newJOP);
				}
//					else
//						return JOP;
			}
			if (ar != null) {
				if (ar.size() == 0) {

					return JOP;
				}
			}

			if (!JOP.containsKey(def)) {
				return JOP;
			}
			ArrayList<Tuple> defList = JOP.get(def);
			for (Tuple i : defList) {
				if (i.getValue() != -1) // NO NULL DEREF (new-1.field = {} not allowed)
				{
					if (JOP.containsKey(new Value(i.getContext(), i.getValue(), field)) && ar != null) // If v6:null occurs on RHS after 1st
																			// iteration, V6 will map to -1 in JOP but
																			// it's (SEE REASON)
					{
						HashSet<Tuple> hs = new HashSet<Tuple>();
						hs.addAll(JOP.get(new Value(i.getContext(), i.getValue(), field)));
						hs.addAll(ar);
						ArrayList<Tuple> temp = new ArrayList<Tuple>();
						temp.addAll(hs);
						newJOP.put(new Value(i.getContext(), i.getValue(), field), temp);
					} else if (symbolTable.isNullConstant(snew.getUse(1))) // RHS NULL, Everything will point to -1
					{
						ArrayList<Tuple> temp = new ArrayList<Tuple>();
						temp.add(new Tuple(-1,""));
						newJOP.put(new Value(i.getContext(), i.getValue(), field), temp);
					} else {
						newJOP.put(new Value(i.getContext(), i.getValue(), field), ar);
					}
				}
			}

		}
		return newJOP;
	}

	private HashMap<Value, ArrayList<Tuple>> join(HashMap<Value, ArrayList<Tuple>> JOP,
			HashMap<Value, ArrayList<Tuple>> newJOP) {

		for (Value x : newJOP.keySet()) {
			if (!JOP.containsKey(x)) {
				JOP.put(x, newJOP.get(x));
			} else {
				ArrayList<Tuple> ar = new ArrayList<Tuple>();
				ar.addAll(newJOP.get(x));
				ar.addAll(JOP.get(x));
				HashSet<Tuple> hs = new HashSet<Tuple>();
				hs.addAll(ar);
				ar = new ArrayList<Tuple>();
				ar.addAll(hs);
				JOP.put(x, ar);
			}
		}
		return JOP;
	}

	private boolean OverApprox(HashMap<Value, ArrayList<Tuple>> JOP, HashMap<Value, ArrayList<Tuple>> newJOP) {
		if (JOP.equals(newJOP)) {
			return false;
		}
		return true;
	}

	private void printColumns(HashMap<Integer, HashMap<Value, ArrayList<Tuple>>> columnMap) {
		// TODO Auto-generated method stub
		System.out.println("Columns");
		for (Integer i: columnMap.keySet())
		{
			System.out.println("c"+i+":");
			printJop(columnMap.get(i));
		}		
	}
	private void printInterProceduralResult(
			HashMap<CGNode, HashMap<Edge, HashMap<Integer, HashMap<Value, ArrayList<Tuple>>>>> result) {
		for(CGNode method: result.keySet())
		{
			System.out.println("Method under consideration:"+method.getMethod().getName());
			for (Edge e : result.get(method).keySet()) {
				System.out.println();
				System.out.println("BB: " + e.src() + " to " + e.dest());
				for(Integer col: result.get(method).get(e).keySet()) {
					System.out.println("	c"+col+":");
					printJop(result.get(method).get(e).get(col),method);
				}
			}			
		}		
	}
	private void printJop(HashMap<Value, ArrayList<Tuple>> jOP) {
		for (Value i : jOP.keySet()) {
			System.out.println();
			System.out.print("		" + i.toString() + " -> {");

			for (int j = 0; j < jOP.get(i).size(); j++) {
				if (j == jOP.get(i).size() - 1)
					if (jOP.get(i).get(j).getValue() == -1)
						System.out.print("NULL");
					else
						System.out.print(jOP.get(i).get(j) + "");
				else if (jOP.get(i).get(j).getValue() == -1)
					System.out.print("NULL, ");
				else
					System.out.print(jOP.get(i).get(j) + ", ");
			}
			System.out.println("}");
		}
	}
	private void printJop(HashMap<Value, ArrayList<Tuple>> jOP, CGNode method) {
		for (Value i : jOP.keySet()) {
			System.out.println();
			System.out.print("		" + i.toString() + " -> {");

			for (int j = 0; j < jOP.get(i).size(); j++) {
				if (j == jOP.get(i).size() - 1)
					if (jOP.get(i).get(j).getValue() == -1)
						System.out.print("NULL");
					else
						System.out.print(jOP.get(i).get(j) + "");
				else if (jOP.get(i).get(j).getValue() == -1)
					System.out.print("NULL, ");
				else
					System.out.print(jOP.get(i).get(j) + ", ");
			}
			System.out.println("}");
		}
	}
}
