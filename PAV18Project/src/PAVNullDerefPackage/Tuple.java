package PAVNullDerefPackage;

public class Tuple {

	private int value;
	private String context;
	
	public Tuple(int value, String context) {
		this.value = value;
		this.context = context;				
	}
	
	public int getValue()
	{
		return value;
	}
	
	public String getContext()
	{
		return context;
	}
	@Override
	public int hashCode() {
		return value+context.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null)
		{
			return false;
		}
		return ((Tuple) o).context.equals(context) && ((Tuple) o).value == value;
	}
	public String toString() {
		return context+".new"+value;
	};
}
