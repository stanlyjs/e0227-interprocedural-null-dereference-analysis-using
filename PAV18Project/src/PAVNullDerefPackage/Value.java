package PAVNullDerefPackage;

public class Value{

	boolean isXVal;
	int x;
	String field;
	String classname;
	String context;
	
	public Value(int x) {
		this.x = x;
		isXVal = true;			
	}
	
	public Value(String context, int x, String field) {
		this.x = x;
		this.field = field;
		this.context = context;
		isXVal = false;			
	}
	
	public Value(String classname, String field) {
		this.classname = classname;
		this.field = field;
	}
	@Override
	public int hashCode()
	{
		if(field!=null)
		return x + field.hashCode();
		return x;
	}
	@Override
	public String toString()
	{
		if(classname !=null)
		{
			return "["+classname+"]."+field;
		}		
		if(isXVal)
		{
			return "v" + x;
		}
		else
		{
			if(field.equals(""))
			{
				return context+".new" + x ;
			}
			return context+".new" + x +  "." + field;
		}
	}
	@Override
	public boolean equals(Object o)
	{
		if(o==null)
		{
			return false;
		}
		if(classname !=null)
		{
			return classname.equals(((Value)o).classname) && field.equals(((Value)o).field);
		}
		if(isXVal)
		{
			return x == ((Value)o).x;
		}
		else
		{
			return x == ((Value)o).x && field.equals(((Value)o).field) && context.equals(((Value)o).context);
		}
	}
}