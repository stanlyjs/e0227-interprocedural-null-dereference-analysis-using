package PAVNullDerefPackage;

public class Edge{
	
	private int src,dest;
	
	Edge(int src, int dest)
	{
		this.src = src;
		this.dest = dest;
	}
	
	public int src()
	{
		return src;
	}
	
	public int dest() 
	{	
		return dest;
	}
	@Override
	public int hashCode()
	{
		return 10*src+dest;
	}

	@Override
	public String toString() {
		return "("+src+", "+dest+")";
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o==null)
		{
			return false;
		}
		
		return src == ((Edge)o).src() && dest == ((Edge)o).dest();
	}
}